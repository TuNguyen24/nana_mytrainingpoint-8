var mailer = require('./../models/mailer.js');
var config = require('config');
var email = config.get('email');


exports.GetTrangChu = function(req,res){
	res.render('./admin/trangchu',{user:req.user});
}
exports.GetDanhSachSV = function(req,res)
{
    //lấy danh sách sinh viên để hiển thị
    req.getConnection(function(err,connection){
        var query = connection.query("select * from sinhvien;",function(err,data){
            if(err)
            {
                console.log(err);
                res.render('LoiHeThong');
            }
            else
            {
                //console.log(data[0].GioiTinh[0]);
                res.render('./admin/danh-sach-sv',{ds_sinhVien:data});
            }
        })
    })
}
exports.GetThemSV = function(req,res){
    //xem admin có quyền thêm sv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa sv k?
                if(data[0].SuaSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                 //lấy danh sách khoa để bỏ vào phần tùy chọn cho khoa của sv
                    req.getConnection(function(err,connection){
                        var query = connection.query("select MaKhoa,Ten from khoa;",function(err,data){
                            if(err){
                                console.log(err);
                                res.redirect('/admin/danh-sach-sv');
                            }
                            else{
                                res.render('./admin/them-sv',{khoa:data});
                            }
                        });
                    })   
                }
            }
        })
    })
}
exports.PostThemSV = function(req,res){
    //xem admin có quyền thêm sv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa sv k?
                if(data[0].SuaSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    //console.log(input);
                     req.getConnection(function(err,connection) {
                         var query = connection.query("insert into SINHVIEN (MSSV,CMND,Ten,DRL,MaKhoa,MaLop,NamNhapHoc,SDT,Email,GioiTinh,NgaySinh,NoiSinh,DiaChi,MatKhau)"
                             +"values('"
                             +input.MSSV+"','"
                             +input.CMND+"',N'"
                             +input.Ten+"',"
                             +"0,'"
                             +input.MaKhoa+"','"
                             +input.MaLop+"',"
                             +input.NamNhapHoc+",'"
                             +input.SDT+"','"
                             +input.email+"',"
                             +input.GioiTinh+",'"
                             +input.NgaySinh+"',N'"
                             +input.NoiSinh+"',N'"
                             +input.DiaChi+"','"
                             +input.CMND+"');",function(err,data){
                                 if(err)
                                 {
                                     console.log(err);
                                     res.render('LoiHeThong');
                                 }
                                 else
                                     res.redirect('/admin/danh-sach-sv');
                         });
                 
                         // console.log(query.sql);
                     })
                }
            }
        })
    })
}
exports.GetSuaSV = function(req,res){
    //xem admin có quyền thêm sv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa sv k?
                if(data[0].SuaSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("select * from sinhvien where MSSV='"+id+"';",function(err,datasv){
                            if(err){
                                console.log(err);
                                res.redirect('/admin/danh-sach-sv');
                            }
                            else{
                                req.getConnection(function(err,connection){
                                    var query = connection.query("select MaKhoa,Ten from khoa;",function(err,datakhoa){
                                        if(err){
                                            console.log(err);
                                            res.redirect('/admin/danh-sach-sv');
                                        }
                                        else{
                                            //console.log(data);
                                            res.render('./admin/sua-sv',{sv:datasv,khoa:datakhoa});
                                        }
                                    })
                                })
                            }
                        })
                    })
                }
            }
        })
    })
}
exports.PostSuaSV = function(req,res){
    //xem admin có quyền thêm sv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa sv k?
                if(data[0].SuaSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("update sinhvien set "
                            +"MSSV ='"+input.MSSV
                            +"',CMND='"+input.CMND
                            +"',Ten=N'"+input.Ten
                            +"',MaKhoa='"+input.MaKhoa
                            +"',MaLop='"+input.MaLop
                            +"',NamNhapHoc="+input.NamNhapHoc
                            +",SDT='"+input.SDT
                            +"',Email='"+input.email
                            +"',GioiTinh="+input.GioiTinh
                            +",NgaySinh='"+input.NgaySinh
                            +"',NoiSinh='"+input.NoiSinh
                            +"',DiaChi='"+input.DiaChi
                            +"',MatKhau='"+input.MatKhau
                            +"' where MSSV='"+id+"';",function(err,data){
                                if(err){
                                    console.log(err);
                                    res.render('LoiHeThong');
                                }
                                else{
                                    res.redirect('/admin/danh-sach-sv');
                                }
                        });
                    })
                }
            }
        })
    })
}
exports.GetXoaSV = function(req,res){
    //xem admin có quyền thêm sv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa sv k?
                if(data[0].SuaSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("delete from sinhvien where MSSV='"+id+"';",function(err,data){
                            if(err){
                                console.log(err);
                                res.render('LoiHeThong');
                            }
                            else{
                                res.redirect('/admin/danh-sach-sv');
                            }
                        })
                    })
                }
            }
        })
    })
}

exports.GetDanhSachAD = function(req,res){
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin;",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else
            {
                res.render('./admin/danh-sach-ad',{ds_ad:data});
            }
        })
    })
}
exports.GetThemAD = function(req,res){
    //xem admin có quyền thêm ad hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa ad k?
                if(data[0].SuaAdmin[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{ //còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    res.render('./admin/them-ad');
                }
            }
        })
    })
}
exports.PostThemAD = function(req,res){
    //xem admin có quyền thêm ad hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa ad k?
                if(data[0].SuaAdmin[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    //console.log(input);
                    req.getConnection(function(err,connection) {
                        var query = 
                        connection.query("insert into admin (Ten,NamSinh,AdminRoot,ThemSV,ThemHD,ThemAdmin,ThemTroLiSV,SuaSV,SuaHD,SuaAdmin,SuaTroLiSV,XoaSV,XoaHD,XoaAdmin,XoaTroLiSV,TaiKhoan,MatKhau)"
                            +" values('"
                            +input.Ten+"',"
                            +input.NamSinh+","
                            +input.adminroot+","
                            +input.suasv+","
                            +input.suahd+","
                            +input.suaad+","
                            +input.suatlsv+",'"
                            +input.taikhoan+"','"
                            +input.matkhau+"');",function(err,data){
                                if(err)
                                {
                                    console.log(err);
                                    res.render('LoiHeThong');
                                }
                                else
                                    res.redirect('/admin/danh-sach-ad');
                        });
                
                       // console.log(query.sql);
                    })
                }
            }
        })
    })
}

exports.GetSuaAD = function(req,res){
    //xem admin có quyền thêm ad hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa ad k?
                if(data[0].SuaAdmin[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("select * from admin where TaiKhoan='"+id+"';",function(err,data){
                            if(err){
                                console.log(err);
                                res.redirect('/admin/danh-sach-ad');
                            }
                            else{
                                //console.log(data);
                                res.render('./admin/sua-ad',{ad:data});
                            }
                        })
                    })
                }
            }
        })
    })
}
exports.PostSuaAD = function(req,res){
    //xem admin có quyền thêm ad hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa ad k?
                if(data[0].SuaAdmin[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("update admin set "
                            +"Ten =N'"+input.Ten
                            +"',NamSinh="+input.NamSinh
                            +",AdminRoot ="+input.adminroot
                            +",SuaSV ="+input.suasv
                            +",SuaHD ="+input.suahd
                            +",SuaAdmin ="+input.suaad
                            +",SuaTroLiSV ="+input.suatlsv
                            +",TaiKhoan ='"+input.taikhoan
                            +"',MatKhau='"+input.matkhau
                            +"' where TaiKhoan='"+id+"'",function(err,data){
                                if(err){
                                    console.log(err);
                                    res.render('LoiHeThong');
                                }
                                else{
                                    res.redirect('/admin/danh-sach-ad');
                                }
                        });
                    })
                }
            }
        })
    })
}
exports.GetXoaAd = function(req,res){
    //xem admin có quyền thêm ad hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa ad k?
                if(data[0].SuaAdmin[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("delete from admin where TaiKhoan='"+id+"';",function(err,data){
                            if(err){
                                console.log(err);
                                res.render('LoiHeThong');
                            }
                            else{
                                res.redirect('/admin/danh-sach-ad');
                            }
                        })
                    })
                }
            }
        })
    })
}

exports.GetDanhSachHD = function(req,res){
    req.getConnection(function(err,connection){
        var query = connection.query("select * from hoatdong;",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.render('./admin/danh-sach-hd',{ds_hd:data});
            }
        })
    })
}
exports.GetThemHD = function(req,res){
    //xem admin có quyền thêm hd hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa hd k?
                if(data[0].SuaHD[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    req.getConnection(function(err,connection){
                        var query = connection.query("select MaLoai,Ten from loaihoatdong;",function(err,data){
                            if(err){
                                console.log('/');
                                res.redirect('/admin/danh-sach-hd');
                            }
                            else{
                                res.render('./admin/them-hd',{ds_ML:data});
                            }
                        })
                    })
                }
            }
        })
    })
}
exports.PostThemHD = function(req,res){
    //xem admin có quyền thêm hd hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa hd k?
                if(data[0].SuaHD[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    //console.log(input);
                    req.getConnection(function(err,connection) {
                        var query = 
                        connection.query("insert into hoatdong(MaHD,MaLoai,Ten,SoDRLDatDuoc,NgayToChuc,DiaDiemToChuc,SoLuongThamGia)"
                            +" values('"
                            +input.MaHD+"','"
                            +input.MaLoai+"',N'"
                            +input.Ten+"',"
                            +input.SoDRLDatDuoc+",'"
                            +input.NgayToChuc+"','"
                            +input.DiaDiemToChuc+"',"
                            +input.SLTG+");",function(err,data){
                                if(err)
                                {
                                    console.log(err);
                                    res.render('LoiHeThong');
                                }
                                else
                                    res.redirect('/admin/danh-sach-hd');
                        });
                
                       // console.log(query.sql);
                    })
                }
            }
        })
    })
}
exports.GetSuaHD = function(req,res){
    //xem admin có quyền thêm hd hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa hd k?
                if(data[0].SuaHD[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("select * from hoatdong where MaHD='"+id+"';",function(err,datahd){
                            if(err){
                                console.log(err);
                                res.redirect('/admin/danh-sach-hd');
                            }
                            else{
                                req.getConnection(function(err,connection){
                                    var query = connection.query("select MaLoai,Ten from loaihoatdong;",function(err,datalhd){
                                        if(err){
                                            console.log(err);
                                            res.redirect('/danh-sach-hd');
                                        }
                                        else{
                                            //console.log(data);
                                            res.render('./admin/sua-hd',{hd:datahd,dslhd:datalhd});
                                        }
                                    })
                                })
                            }
                        })
                    })
                }
            }
        })
    })
}
exports.PostSuaHD = function(req,res){
    //xem admin có quyền thêm hd hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa hd k?
                if(data[0].SuaHD[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("update hoatdong set "
                            +"MaHD ='"+input.MaHD
                            +"',MaLoai='"+input.MaLoai
                            +"',Ten=N'"+input.Ten
                            +"',SoDRLDatDuoc="+input.SoDRLDatDuoc
                            +",NgayToChuc='"+input.NgayToChuc
                            +"',DiaDiemToChuc=N'"+input.DiaDiemToChuc
                            +"',SoLuongThamGia="+input.SLTG
                            +" where MaHD='"+id+"';",function(err,data){
                                if(err){
                                    console.log(err);
                                    res.render('LoiHeThong');
                                }
                                else{
                                    res.redirect('/admin/danh-sach-hd');
                                }
                        });
                    })
                }
            }
        })
    })
}
exports.GetXoaHD = function(req,res){
    //xem admin có quyền thêm hd hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa hd k?
                if(data[0].SuaHD[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("delete from hoatdong where MaHD='"+id+"';",function(err,data){
                            if(err){
                                console.log(err);
                                res.render('LoiHeThong');
                            }
                            else{
                                res.redirect('/admin/danh-sach-hd');
                            }
                        })
                    })
                }
            }
        })
    })
}

exports.GetDanhSachDVPT = function(req,res){
    req.getConnection(function(err,connection){
        var query = connection.query("select * from donviphutrach;",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.render('./admin/danh-sach-dvpt',{ds_DVPT:data});
            }
        })
    })
}
exports.GetThemDVPT = function(req,res){
    req.getConnection(function(err,connection){
        var query = connection.query("select MaHD,Ten from hoatdong;",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                console.log(data);
                res.render('./admin/them-dvpt',{ds_MaHD:data});
            }
        })
    })
}
exports.PostThemDVPT = function(req,res){
    var input = JSON.parse(JSON.stringify(req.body));
    //console.log(input);
    req.getConnection(function(err,connection) {
        var query = 
        connection.query("insert into donviphutrach(MaHD,Cap,Ten)"
            +" values('"
            +input.MaHD+"',N'"
            +input.Cap+"',N'"
            +input.Ten+"');",function(err,data){
                if(err)
                {
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else
                    res.redirect('/danh-sach-dvpt');
        });

       // console.log(query.sql);
    })
}
exports.GetSuaDVPT = function(req,res){
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("select * from DONVIPHUTRACH where MaHD='"+id+"';",function(err,datadvpt){
            if(err){
                console.log(err);
                res.redirect('/admin/danh-sach-dvpt');
            }
            else{
                req.getConnection(function(err,connection){
                    var query = connection.query("select MaHD,Ten from hoatdong;",function(err,datahd){
                        if(err){
                            console.log(err);
                            res.redirect('/admin/danh-sach-dvpt');
                        }
                        else{
                            //console.log(data);
                            res.render('./admin/sua-dvpt',{dvpt:datadvpt,dshd:datahd});
                        }
                    })
                })
            }
        })
    })
}
exports.PostSuaDVPT = function(req,res){
    var input = JSON.parse(JSON.stringify(req.body));
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("update donviphutrach set "
            +"MaHD ='"+input.MaHD
            +"',Cap=N'"+input.Cap
            +"',Ten=N'"+input.Ten
            +"' where MaHD='"+id+"';",function(err,data){
                if(err){
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else{
                    res.redirect('/admin/danh-sach-dvpt');
                }
        });
    })
}
exports.GetXoaDVPT = function(req,res){
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("delete from donviphutrach where MaHD='"+id+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/admin/danh-sach-dvpt');
            }
        })
    })
}

exports.GetDanhSachLHD = function(req,res){
    req.getConnection(function(err,connection){
        var query = connection.query("select* from loaihoatdong;",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.render('./admin/danh-sach-lhd',{ds_lhd:data});
            }
        })
    })
}
exports.GetThemLHD = function(req,res){
    res.render('./admin/them-lhd');
}
exports.PostThemLHD = function(req,res){
    var input = JSON.parse(JSON.stringify(req.body));
    //console.log(input);
    req.getConnection(function(err,connection) {
        var query = 
        connection.query("insert into loaihoatdong(MaLoai,Ten,TongDiemToiDa)"
            +" values('"
            +input.MaLoai+"',N'"
            +input.Ten+"',"
            +input.TDTD+");",function(err,data){
                if(err)
                {
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else
                    res.redirect('/admin/danh-sach-lhd');
        });

       // console.log(query.sql);
    })
}
exports.GetSuaLHD = function(req,res){
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("select * from loaihoatdong where MaLoai='"+id+"';",function(err,data){
            if(err){
                console.log(err);
                res.redirect('/admin/danh-sach-lhd');
            }
            else{
                //console.log(data);
                res.render('./admin/sua-lhd',{lhd:data});
            }
        })
    })
}
exports.PostSuaLHD = function(req,res){
    var input = JSON.parse(JSON.stringify(req.body));
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("update loaihoatdong set "
            +"MaLoai ='"+input.MaLoai
            +"',Ten=N'"+input.Ten
            +"',TongDiemToiDa="+input.TDTD
            +" where MaLoai='"+id+"';",function(err,data){
                if(err){
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else{
                    res.redirect('/admin/danh-sach-lhd');
                }
        });
    })
}
exports.GetXoaLHD = function(req,res){
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("delete from loaihoatdong where MaLoai='"+id+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/admin/danh-sach-lhd');
            }
        })
    })
}

exports.GetDanhSachKHOA = function(req,res){
    req.getConnection(function(err,connection){
        var query = connection.query("select*from khoa;",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.render('./admin/danh-sach-khoa',{ds_khoa:data});
            }
        })
    })
}
exports.GetThemKHOA = function(req,res){
    res.render('./admin/them-khoa');
}
exports.PostThemKHOA = function(req,res){
    var input = JSON.parse(JSON.stringify(req.body));
    //console.log(input);
    req.getConnection(function(err,connection) {
        var query = 
        connection.query("insert into khoa(MaKhoa,Ten,SDT,Email,Phong)"
            +" values('"
            +input.MaKhoa+"',N'"
            +input.Ten+"','"
            +input.SDT+"','"
            +input.email+"','"
            +input.Phong+"');",function(err,data){
                if(err)
                {
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else
                    res.redirect('/admin/danh-sach-khoa/them-khoa');
        });

       // console.log(query.sql);
    })
}
exports.GetSuaKHOA = function(req,res){
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("select * from khoa where MaKhoa='"+id+"';",function(err,data){
            if(err){
                console.log(err);
                res.redirect('/admin/danh-sach-khoa');
            }
            else{
                //console.log(data);
                res.render('./admin/sua-khoa',{khoa:data});
            }
        })
    })
}
exports.PostSuaKHOA = function(req,res){
    var input = JSON.parse(JSON.stringify(req.body));
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("update khoa set "
            +"MaKhoa ='"+input.MaKhoa
            +"',Ten=N'"+input.Ten
            +"',SDT='"+input.SDT
            +"',Email='"+input.email 
            +"',Phong='"+input.Phong
            +"' where MaKhoa='"+id+"';",function(err,data){
                if(err){
                    console.log(err);
                    res.render('LoiHeThong');
                }
                else{
                    res.redirect('/admin/danh-sach-khoa');
                }
        });
    })
}
exports.GetXoaKHOA = function(req,res){
    var id = req.params.id;
    req.getConnection(function(err,connection){
        var query = connection.query("delete from khoa where MaKhoa='"+id+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.redirect('/admin/danh-sach-khoa');
            }
        })
    })
}

exports.GetDanhSachTLSV = function(req,res){
    req.getConnection(function(err,connection){
        var query = connection.query("select * from trolisv;",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{
                res.render('./admin/danh-sach-tlsv',{ds_tlsv:data});
            }
        })
    })
}
exports.GetThemTLSV = function(req,res){
    //xem admin có quyền thêm tlsv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa tlsv  k?
                if(data[0].SuaTroLiSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    req.getConnection(function(err,connection){
                        var query = connection.query("select MaKhoa,Ten from khoa;",function(err,data){
                            if(err){
                                console.log(err);
                                res.redirect('/admin/danh-sach-tlsv');
                            }
                            else{
                                res.render('./admin/them-tlsv',{ds_khoa:data});
                            }
                        })
                    })
                }
            }
        })
    })
}
exports.PostThemTLSV = function(req,res){
    //xem admin có quyền thêm tlsv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa tlsv  k?
                if(data[0].SuaTroLiSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    //console.log(input);
                    req.getConnection(function(err,connection) {
                        var query = 
                        connection.query("insert into trolisv(MaKhoa,Ten,SDT,Email,PhongLamViec)"
                            +" values('"
                            +input.MaKhoa+"',N'"
                            +input.Ten+"','"
                            +input.SDT+"','"
                            +input.email+"','"
                            +input.PhongLamViec+"');",function(err,data){
                                if(err)
                                {
                                    console.log(err);
                                    res.render('LoiHeThong');
                                }
                                else
                                    res.redirect('/admin/danh-sach-tlsv');
                        });
                
                       // console.log(query.sql);
                    })
                }
            }
        })
    })
}
exports.GetSuaTLSV = function(req,res){
    //xem admin có quyền thêm tlsv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa tlsv  k?
                if(data[0].SuaTroLiSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("select * from TROLISV where Ten=N'"+id+"';",function(err,datatlsv){
                            if(err){
                                console.log(err);
                                res.redirect('/admin/danh-sach-tlsv');
                            }
                            else{
                                req.getConnection(function(err,connection){
                                    var query = connection.query("select MaKhoa,Ten from khoa;",function(err,datakhoa){
                                        if(err){
                                            console.log(err);
                                            res.redirect('/admin/danh-sach-tlsv');
                                        }
                                        else{
                                            //console.log(data);
                                            res.render('./admin/sua-tlsv',{tlsv:datatlsv,ds_khoa:datakhoa});
                                        }
                                    })
                                })
                            }
                        })
                    })
                }
            }
        })
    })
}
exports.PostSuaTLSV = function(req,res){
    //xem admin có quyền thêm tlsv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa tlsv  k?
                if(data[0].SuaTroLiSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var input = JSON.parse(JSON.stringify(req.body));
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("update TROLISV set "
                            +"MaKhoa ='"+input.MaKhoa
                            +"',Ten=N'"+input.Ten
                            +"',SDT='"+input.SDT
                            +"',Email='"+input.email 
                            +"',PhongLamViec='"+input.PhongLamViec
                            +"' where Ten=N'"+id+"';",function(err,data){
                                if(err){
                                    console.log(err);
                                    res.render('LoiHeThong');
                                }
                                else{
                                    res.redirect('/admin/danh-sach-tlsv');
                                }
                        });
                        console.log(query.sql);
                    })
                }
            }
        })
    })
}
exports.GetXoaTLSV = function(req,res){
    //xem admin có quyền thêm tlsv hay k?
    req.getConnection(function(err,connection){
        var query = connection.query("select * from admin where Taikhoan = '"+req.user+"';",function(err,data){
            if(err){
                console.log(err);
                res.render('LoiHeThong');
            }
            else{ //xem admin có quyền sửa tlsv  k?
                if(data[0].SuaTroLiSV[0] == 0) {//nếu k có thì thông báo không cho truy cập
                    res.render('./admin/khongcoquyen');
                }else{//còn nếu có quyền sửa thì xuống dưới thực hiện bth
                    var id = req.params.id;
                    req.getConnection(function(err,connection){
                        var query = connection.query("delete from TROLISV where Ten=N'"+id+"';",function(err,data){
                            if(err){
                                console.log(err);
                                res.render('LoiHeThong');
                            }
                            else{
                                res.redirect('/admin/danh-sach-tlsv');
                            }
                        })
                    })
                }
            }
        })
    })
}